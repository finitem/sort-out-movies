const fs = require('fs')
const path = require('path')
const mkdirp = require('mkdirp')

const startDir = process.argv[2]
const endDir = process.argv[3]

// For each folder in startDir:
//  go into it, then...
//  Repeat
// For each file in dir:
//  if video, move to pwd with startDir swapped for endDir
// That's it, it's super simple.

var movedExts = new Set()
var leftExts = new Set()
var accepted = new Set(['mp4', 'm4v', 'avi', 'mpg', 'mov'])
var moved = 0
var ignored = 0

var doThin = function (startDir, endDir) {
    console.log("DIR: " + startDir)
    console.log("TO: " + endDir)
    fs.readdirSync(startDir).forEach(function (fileName) {
        console.log(fileName)
        var longFileName = path.join(startDir, fileName)
        var longDestName = path.join(endDir, fileName)
        if (fs.lstatSync(longFileName).isDirectory()) {
            console.log(endDir + fileName + "?? -- ??")
            doThin(longFileName, longDestName)
        } else if (fs.lstatSync(longFileName).isFile()) {
            var re = /(?:\.([^.]+))?$/;
            var ext = re.exec(fileName)[1];
            if (accepted.has(ext)) {
                console.log("MOVE: " + longFileName)
                moved += 1
                movedExts.add(ext)
                mkdirp.sync(endDir)
                fs.rename(longFileName, path.join(endDir, fileName))
                // move the file
            } else {
                console.log("IGNORE: " + longFileName)
                ignored += 1
                leftExts.add(ext)
            }
        }
    })
}

doThin(startDir, endDir)
console.log("MOVED: " + [...movedExts].join(" "))
console.log("IGNORED: " + [...leftExts].join(" "))
console.log("MOVE COUNT: " + moved)
console.log("IGNORE COUNT: " + ignored)